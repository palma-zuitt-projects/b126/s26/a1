
//Create a new route for creating a new user when a POST request is sent to the /users endpoint. If either the username or password fields are empty, registration will not take place (return an error message). After that, check for duplicate usernames before creating a new user.

//Add and test the code in this file. Once working, copy index.js from this s26/d1 folder to s26/a1, and push a1 (NOT d1) to Gitlab and send the link via Boodle.


app.post("/users", (req,res) => {
	console.log(req.body)

	User.insertOne({username:String, password:String}, (err, result) => {
		console.log(result)
		if(result.body.username !== null && result.body.password !== null){
			return res.send("Successfully Registered")
		}else if(result.body.username === result.username && result.body.password === result.password){
			return res.send("Duplicate info has found!")
		}else{
			return res.send("Username or Password must not be empty!")
		}
	})
})
